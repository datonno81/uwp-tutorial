﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Media.Imaging;
using System.Collections.ObjectModel;
using static News.Models.NewsJSON;
using System.Text.RegularExpressions;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace News
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private ObservableCollection<RootObject> NewsConnection;
        private ObservableCollection<RootObject> NewsGetFromJSON;
        private int CurrentIndex;

        public MainPage()
        {
            this.InitializeComponent();
            NewsConnection = new ObservableCollection<RootObject>();
            NewsGetFromJSON = new ObservableCollection<RootObject>();
            InitNews("http://tommyprivateguide.com/wp-json/wp/v2/posts?param/data.json");
        }
        private async void InitNews(string URL)
        {
            NewsGetFromJSON.Clear();
            var news = await RootObject.GetNews(URL) as List<RootObject>;
            news.ForEach(it =>
            {
                if(it.author == "1")
                {
                    it.author = "admin";
                }
                if(it.comment_status == "Closed")
                {
                    it.comment_status = "0";
                }
                else
                {
                    it.comment_status = "1";
                }
                it.content.rendered = Regex.Replace(it.content.rendered, "<.*?", "");
                it.content.rendered = Regex.Replace(it.content.rendered, "&nbsp;", "");
                it.content.rendered = Regex.Replace(it.content.rendered, "<.*?>", "");
                if(it.excerpt.rendered.Length > 100)
                {
                    it.excerpt.rendered = it.excerpt.rendered.Substring(0, 100) + "...Read more!!!";
                }
                NewsGetFromJSON.Add(it);
            }
            );

        }
        private void Page_SixeChanged(object sender, SizeChangedEventArgs e)
        {
            var bounds = Window.Current.Bounds;
            double height = bounds.Height;
            double width = bounds.Width;
            SplitViewReadNews.OpenPaneLength = width;
        }

        private void NewsItemGrid_ItemClick(object sender, ItemClickEventArgs e)
        {
            var article = e.ClickedItem as RootObject;
            CurrentIndex = NewsGetFromJSON.IndexOf(article);
            TextBlockViewNewsTitle.Text = article.title.rendered;
            TextBlockDate.Text = "_" + article.date;
            TextBlockViewNewsAuthor.Text = "My" + article.author;
            ImageViewNewsImage.Source = new BitmapImage(new Uri(article.better_featured_image.source_url));
            TextBlockViewNewsContent.Text = article.content.rendered;
            TextBlockComment.Text = article.comment_status + "Comment";
            GetConnection();
            if ((sender as GridView).Name != "GridViewConnection")
                SplitViewReadNews.IsPaneOpen = !SplitViewReadNews.IsPaneOpen;
        }

        private void GetConnection()
        {
            NewsConnection.Clear();
            for(var i = 1; i <=6; i++)
            {
                var index = CurrentIndex + i;
                if (index > NewsGetFromJSON.Count - 1) index -= (NewsGetFromJSON.Count - 1);
                NewsConnection.Add(NewsGetFromJSON[index]);
            }

        }

        private void CombackButton_Click(object sender, RoutedEventArgs e)
        {
            SplitViewReadNews.IsPaneOpen = false;
        }
        private void GridViewConnection_ItemClick(object sender, ItemClickEventArgs e)
        {

        }
    }
}
