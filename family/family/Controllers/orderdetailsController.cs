﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using family;

namespace family.Controllers
{
    public class orderdetailsController : Controller
    {
        private SuperMarketEntities db = new SuperMarketEntities();

        // GET: orderdetails
        public ActionResult Index()
        {
            var orderdetails = db.orderdetails.Include(o => o.product).Include(o => o.order);
            return View(orderdetails.ToList());
        }
        public ActionResult ShowByPriceEach()
        {
            var orderdetail = from p in db.orderdetails
                           orderby p.priceEach ascending
                           select p;
            return View(orderdetail);
        }


        // GET: orderdetails/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            orderdetail orderdetail = db.orderdetails.Find(id);
            if (orderdetail == null)
            {
                return HttpNotFound();
            }
            return View(orderdetail);
        }

        // GET: orderdetails/Create
        public ActionResult Create()
        {
            ViewBag.productCode = new SelectList(db.products, "productCode", "productName");
            ViewBag.orderCode = new SelectList(db.orders, "orderCode", "comments");
            return View();
        }

        // POST: orderdetails/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "orderDCode,productCode,orderCode,priceEach,quantityOrdered")] orderdetail orderdetail)
        {
            if (ModelState.IsValid)
            {
                db.orderdetails.Add(orderdetail);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.productCode = new SelectList(db.products, "productCode", "productName", orderdetail.productCode);
            ViewBag.orderCode = new SelectList(db.orders, "orderCode", "comments", orderdetail.orderCode);
            return View(orderdetail);
        }

        // GET: orderdetails/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            orderdetail orderdetail = db.orderdetails.Find(id);
            if (orderdetail == null)
            {
                return HttpNotFound();
            }
            ViewBag.productCode = new SelectList(db.products, "productCode", "productName", orderdetail.productCode);
            ViewBag.orderCode = new SelectList(db.orders, "orderCode", "comments", orderdetail.orderCode);
            return View(orderdetail);
        }

        // POST: orderdetails/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "orderDCode,productCode,orderCode,priceEach,quantityOrdered")] orderdetail orderdetail)
        {
            if (ModelState.IsValid)
            {
                db.Entry(orderdetail).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.productCode = new SelectList(db.products, "productCode", "productName", orderdetail.productCode);
            ViewBag.orderCode = new SelectList(db.orders, "orderCode", "comments", orderdetail.orderCode);
            return View(orderdetail);
        }

        // GET: orderdetails/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            orderdetail orderdetail = db.orderdetails.Find(id);
            if (orderdetail == null)
            {
                return HttpNotFound();
            }
            return View(orderdetail);
        }

        // POST: orderdetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            orderdetail orderdetail = db.orderdetails.Find(id);
            db.orderdetails.Remove(orderdetail);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
