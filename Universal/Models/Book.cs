﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Universal.Models
{
   public class Book
    {
        public int BookId { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public string ConverImage { get; set; }
    }
    public class BookManager
    {
        public static List<Book> GetBooks()
        {
            var books = new List<Book>();

            books.Add(new Book { BookId = 1, Title = "Vulpate", Author = "Fututum", ConverImage = "Assets/1.png" });
            books.Add(new Book { BookId = 2, Title = "Mazim", Author = "Sequiter que", ConverImage = "Assets/2.png" });
            books.Add(new Book { BookId = 3, Title = "Elit", Author = "Tempor", ConverImage = "Assets/3.png" });
            books.Add(new Book { BookId = 4, Title = "Etiam", Author = "Option", ConverImage = "Assets/4.png" });
            books.Add(new Book { BookId = 5, Title = "Feugait Eros Libex", Author = "Accumsan", ConverImage = "Assets/5.png" });
            books.Add(new Book { BookId = 6, Title = "Nonummy Erat", Author = "Lugunt xaepius", ConverImage = "Assets/6.png" });
            books.Add(new Book { BookId = 7, Title = "Nostrud", Author = "Eleifend", ConverImage = "Assets/7.png" });
            books.Add(new Book { BookId = 8, Title = "Per Modo", Author = "Vero Tation", ConverImage = "Assets/8.png" });
            books.Add(new Book { BookId = 9, Title = "Suscipit Ad", Author = "Jack Tibbles", ConverImage = "Assets/9.png" });
            books.Add(new Book { BookId = 10, Title = "Decima", Author = "Tuffy tibbles", ConverImage = "Assets/10.png" });
            books.Add(new Book { BookId = 11, Title = "Erat", Author = "Volupat", ConverImage = "Assets/11.png" });
            books.Add(new Book { BookId = 12, Title = "Consequat", Author = "Est Possim", ConverImage = "Assets/12.png" });
            books.Add(new Book { BookId = 13, Title = "Aliquip", Author = "Magna", ConverImage = "Assets/13.png" });
            return books;
        }
    }
}
