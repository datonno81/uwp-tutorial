﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObservableCollectionExample.Models
{
    class Contact
    {
        public string FirstName { set; get; }
        public string LastName { set; get; }
        public string AvatarPath { set; get; }
    }
}
